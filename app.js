var roomTypes = [
	{name: "Спальня",				coef: 1},
	{name: "Гостинная",			coef: 213},
	{name: "Кухня",					coef: 541},
	{name: "Ванная комната",coef: 123},
	{name: "Балкон",				coef: 9999}];

var repairTypes = [
	{name: "Капитальный ремонт",	coef : 1},
	{name : "Премиум ремонт",			coef : 3}
];

function getRoomTypes(){
	return _.map(roomTypes, function(roomType){return roomType.name})
}

function getRepairTypes(){
	return _.map(repairTypes, function(repairType){return repairType.name})
}


function Room(){
    this.type = 0;
    this.repairType = 0;
    this.space = 0;
    console.log(this);
}

function Calculator(){
    this.rooms = [];
    this.addRoom = function(room){
			this.rooms.push(room);
    }
    this.calculate = function(room){
    	return room.space*roomTypes[room.type].coef*repairTypes[room.repairType].coef;
    }
}

var calculator = new Calculator();

// =====[ DOM MANIPULATION SHIT] ======

function addSelect (options, value) {
	var select = document.createElement("select")
	options.map(function(label, value){
		var option = document.createElement("option");
		option.value = value;
		option.text = label;
		select.appendChild(option);
	});
	select.className = 'form-control';
	select.value = value;
	return select;
}

document.getElementById('addButton').onclick = function(){
	var room = new Room();
	calculator.addRoom(room);

	document.getElementById("calcul");
	var row = document.createElement('tr');
	calcul.appendChild(row);

	var roomType = addSelect(getRoomTypes(), room.type)
	roomType.onchange = function(event){
		room.type = event.target.value;
		span.innerText = calculator.calculate(room);
	}
	row
		.insertCell(0)
		.appendChild(roomType);

	var repairType = addSelect(getRepairTypes(), room.repairType);
	repairType.onchange = function(event){
		room.repairType = event.target.value;
		span.innerText = calculator.calculate(room);
	}
	row
		.insertCell(1)
		.appendChild(repairType);

	var input = document.createElement('input');
	input.value = room.space;
	input.className = 'form-control';
	input.onchange = function(event){
		room.space = event.target.value;
		span.innerText = calculator.calculate(room);
	}
	row
		.insertCell(2)
		.appendChild(input);

	var span = document.createElement('span');
	span.innerText = calculator.calculate(room);
	row
		.insertCell(3)
		.appendChild(span);
}
